
var request = require('supertest') ,
    should  = require('should') ,

    mitch   = require('mitch') ,
    app     = mitch() ;


var healthcheck = require('..')(app);

app.use(healthcheck.mountPath, healthcheck.server);

describe('Healthcheck', function () {

  it('Responds with timestamp', function (done) {
    request(app)
      .get('/_healthcheck/')
      .expect(200)
      .expect('Content-Type', /plain/)
      .end(done);
  });
  it('Responds to ping', function (done) {
    request(app)
      .get('/_healthcheck/ping')
      .expect(200)
      .expect('Content-Type', /plain/)
      .expect( function (res) {
        res.text.should.equal('pong');
      })
      .end(done);
  });

});
