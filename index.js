
// Export the mount path and server. Warp will looked for 'server' when mounting
module.exports = function(app) {

  return {
    mountPath  : '/_healthcheck' ,
    server     : require('./server.js')(app)
  };

};
