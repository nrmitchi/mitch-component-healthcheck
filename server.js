
module.exports = function (app) {
  var router       = app.Router() ;

  // Create somewhere on warp for the module to save stuff (that typically only applies to it)
  app.healthcheck = app.healthcheck || {};

  router.get('/', function (req, res, next) {
    res.header("Content-Type", "text/plain");
    res.end(new Date().getTime().toString());
  });
  router.get('/ping', function (req, res, next) {
    res.header("Content-Type", "text/plain");
    res.end('pong');
  });

  return router;

};
